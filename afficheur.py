from textblob import TextBlob
from twitter_collect.fonc_3 import *
import seaborn as sns


def affiche_stat(nom_candidat1, nom_candidat2):
    """affiche les % de tweets positifs et négatifs pour les 2 candidats"""
    # crée les deux graphique
    sns.set(style="white", context="talk")
    f, (ax1, ax2) = plt.subplots(2, 1, figsize=(7, 5), sharex=True)


    label_x_graph1 = np.array([nom_candidat1, nom_candidat2])
    label_x_graph2 = np.array([nom_candidat1, nom_candidat2])

    # récupère les % de tweet positifs et négatifs pour les 2 candidats
    pos1, neu1, neg1 = number_to_stat(1)
    pos2, neu2, neg2 = number_to_stat(2)

    # array de valeurs à afficher
    values_graph1 = np.array([pos1, pos2])
    values_graph2 = np.array([neg1, neg2])

    #ajoute les valeurs sur le premier graphique
    sns.barplot(x=label_x_graph1, y=values_graph1, palette="rocket", ax=ax1)
    ax1.axhline(0, color="k", clip_on=False)
    ax1.set_ylabel("% positifs")

    #ajoute les valeurs sur le 2e graphique
    sns.barplot(x=label_x_graph1, y=values_graph2, palette="rocket", ax=ax2)
    ax2.axhline(0, color="k", clip_on=False)
    ax2.set_ylabel("% negatifs")

    # affiche le graphique
    sns.despine(bottom=True)
    plt.setp(f.axes)
    plt.tight_layout(h_pad=2)

affiche_stat("Macron", "Clinton")