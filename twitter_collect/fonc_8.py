from textblob import TextBlob
from twitter_collect.fonc_3 import *


def string_to_stat(string):
    data = transform_to_dataframe(collect(string))

    # liste_mots=[]
    txt = data['tweet_textual_content']
    return stat(txt)


'''for sentence in txt:
    mots=TextBlob(sentence)
    liste_mots.append(mots.words)

#puis on remplace chaque mot par sa version lemmatisée
for n in range (len(liste_mots)):
    mot=liste_mots[n]
    liste_mots[n]=mot.lemmatize()

def unique(liste):
    # cette fonction supprime renvoie une liste sans doublon
    res=[]
    for mot in liste:
        if not mot in res:
            res.append(mot)
    return res

liste_mots=unique(liste_mots)'''


def stat(txt):
    # détermine le % de tweets positifs, négatifs, ou neutres dans txt

    pos_tweets = []
    neg_tweets = []
    neu_tweets = []
    for sentence in txt:
        sentence = TextBlob(sentence)
        if sentence.sentiment.polarity > 0.2:
            pos_tweets.append(sentence)
        elif sentence.sentiment.polarity < -0.2:
            neg_tweets.append(sentence)
        else:
            neu_tweets.append(sentence)
    print("Percentage of positive tweets: {}%".format(len(pos_tweets) * 100 / len(txt)))
    print("Percentage of neutral tweets: {}%".format(len(neu_tweets) * 100 / len(txt)))
    print("Percentage de negative tweets: {}%".format(len(neg_tweets) * 100 / len(txt)))
    return len(pos_tweets) * 100 / len(txt), len(neu_tweets) * 100 / len(txt), len(neg_tweets) * 100 / len(txt)


def number_to_stat(num_candidate):
    #détermine le % de tweets positifs, négatifs et neutres pour le candidat n° num_candidate

    res = []
    file_path = 'CandidateData'

    #ajoute tous les tweets ayant les hashtags du candidats à res
    with open('hashtag_candidate_' + str(num_candidate) + '.txt', 'r') as fichier:
        for line in fichier:
            res.append(collect(line))

    #ajoute tous les tweets ayant les mots clés du candidats
    #certains tweets sont donc ajoutés plusieurs fois à res
    with open('keywords_candidate_' + str(num_candidate) + '.txt', 'r') as fichier:
        for line in fichier:
            res.append(collect(line))

    dict_list = []


    for tweets in res:
        for tweet in tweets:
            dict_list.append(tweet_to_dict(tweet))

    data = pd.DataFrame(dict_list)
    txt = data['tweet_textual_content']

    return stat(txt)
