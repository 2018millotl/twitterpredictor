import seaborn as sns
import matplotlib.pyplot as plt
from twitter_collect.fonc_8 import *
from textblob import Word

def filtrer(l1,l2):
    l1_filtree=[]
    l2_filtree=[]
    for i in range(len(l1)):
        if l1[i]!= 0 or l2[i]!=0:
            l1_filtree.append(l1[i])
            l2_filtree.append(l2[i])
    return l1_filtree,l2_filtree


def number_to_pol_sub(num_candidate):

     # renvoie 2 liste polarity, subjectivity, qui représentent à quel point les tweets concernant num_candidate sont polarisés et subjectifs

    list_list_tweets = []

    # récupère des listes de tweets par hashtag puis par mot-clé
    with open('hashtag_candidate_' + str(num_candidate) + '.txt', 'r') as fichier:
        for line in fichier:
            list_list_tweets.append(collect(line))

    with open('keywords_candidate_' + str(num_candidate) + '.txt', 'r') as fichier:
        for line in fichier:
            list_list_tweets.append(collect(line))

    dict_list = []

    # transforme la liste de liste de tweets en une liste de dictionnaire contenant les informations de chaque tweet
    for tweets in list_list_tweets:
        for tweet in tweets:
            dict_list.append(transform_to_dict(tweet))

    data = pd.DataFrame(dict_list)
    txt = data['tweet_textual_content']
    subjectivity=[]
    polarity=[]

    # crée 2 listes polarité et subjectivité, utilisés plus loin comme abscisses et ordonnées des tweets sur le graphe
    for sentence in txt :
        sentence = TextBlob(sentence)
        subjectivity.append(sentence.sentiment.subjectivity)
        polarity.append(sentence.sentiment.polarity)

    polarity,subjectivity=filtrer(polarity,subjectivity)
    print(len(polarity))
    pol=sorted(polarity)
    med_pol=pol[len(pol)//2]
    sub=sorted(subjectivity)
    med_sub=sub[len(subjectivity)//2]
    return polarity,subjectivity,sum(polarity)/len(polarity),sum(subjectivity)/len(subjectivity),med_pol,med_sub

def trace():
    polarity, subjectivity, moy_pol, moy_sub, med_pol, med_sub = number_to_pol_sub(1)
    plt.plot(polarity,subjectivity, linestyle= 'none',marker='1',color='red')
    polarity2, subjectivity2, moy_pol2, moy_sub2, med_pol2, med_sub2 = number_to_pol_sub(2)
    plt.plot(polarity2, subjectivity2, linestyle='none',marker='1',color='blue')
    plt.plot([moy_pol], [moy_sub], marker='X', color='yellow')
    plt.plot([med_pol], [med_sub], marker='o', color='yellow')
    plt.plot([moy_pol2], [moy_sub2], marker='X', color='green')
    plt.plot([med_pol2], [med_sub2], marker='o', color='green')

    # trace les nuages de points polartité/subjectivité pour les candidats 1 et 2


    plt.xlabel('polarity (-1,1)')
    plt.ylabel('subjectivity (0,1)')

    plt.show()
