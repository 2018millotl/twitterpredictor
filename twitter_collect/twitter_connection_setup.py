import tweepy
# We import our access keys:
from twitterPredictor.credentials import *


def twitter_setup():
    """
    Utility function to setup the Twitter's API
    with an access keys provided in a file credentials.py
    :return: the authentified API
    """
    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    # Return API with authentication:
    api = tweepy.API(auth)
    return api


def collect(text):
    #affiche le texte des tweets français contenant text
    connexion = twitter_setup()
    tweets = connexion.search(text, language="french", rpp=100)
    for tweet in tweets:
        print(tweet.text)


def collect_by_user(user_id):
    #récupère le texte des tweets d'un utilisateur avec user_id, et l'affiche.
    connexion = twitter_setup()
    statuses = connexion.user_timeline(id=user_id, count=70)
    for status in statuses:
        print(status.text)
    return statuses


from tweepy.streaming import StreamListener


class StdOutListener(StreamListener):
    # qu'est ce que c'est tout ça?
    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        if str(status) == "420":
            print("blaze it")
            print(status)
            print("You exceed a limited number of attempts to connect to the streaming API")
            return False
        else:
            return True


def collect_by_streaming(candidate_name):
    # récupère des tweets au fur à mesure je crois?

    connexion = twitter_setup()
    listener = StdOutListener()
    stream = tweepy.Stream(auth=connexion.auth, listener=listener)
    stream.filter(track=[candidate_name])
