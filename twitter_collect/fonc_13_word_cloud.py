from textblob import TextBlob
from twitter_collect.fonc_3 import *
import twitter_collect


def word_to_color(mot):
    """associe une couleur à un mot en fonction de sa polarité"""

    mot_blob = TextBlob(mot)
    polarity = mot_blob.sentiment

    if polarity < -0.5:
        color = "red"

    if -0.5 <= polarity < 0:
        color = "yellow"

    if 0 < polarity <= 0.5:
        color = "green"

    if 0.5 < polarity:
        color = "blue"

    return color


def flatten(liste_liste):
    """ renvoie une liste contenant tous les éléments d'une liste de listes """

    liste_plate = []

    for liste in liste_liste:
        for element in liste:
            liste_plate.append(element)

    return liste_plate


def split_multiple(string, texte):
    """renvoie une liste de string à partir de texte comme split, sauf qu'on prend plusieurs séparateurs"""

    def split_liste_mot(string, liste_mots):
        """prend en argument string : les différents séparateurs, et liste_mots: une liste de mots déjà splitée"""

        # si il n'y a plus de séparateurs, on a fini
        if string == "":
            return liste_mots

        else:
            # sinon, on split chaque mot de liste_mot par le premier caractère,
            # on aplatit la liste obtenue, et on recommence avec le reste de la string
            lettre, fin_string = string[0], string[1:]

            for i in range(len(liste_mots)):
                liste_mots[i] = filter(lambda x: x!= "", liste_mots[i].split(lettre))

            return split_liste_mot(fin_string, flatten(liste_mots))

    if string != "":
        liste_mots = texte.split(string[0])
        return split_liste_mot(string[1:], liste_mots)

    else:
        raise Exception("impossible de spliter : string vide")


def ensemble_mots_tweets(data):
    """renvoie une liste avec tous les mots de la data"""

    mots = []

    for ind in data.index:
        tweet = data["tweet_textual_content"][ind]
        mots += split_multiple(" .;:,?!", tweet)

    return mots


