import seaborn as sns
from twitter_collect.fonc_8 import *


data = transform_to_dataframe(collect("donaldtrump OR Trump OR trump"))

sns.set(style="white", context="talk")
f, (ax1, ax2) = plt.subplots(2, 1, figsize=(7, 5), sharex=True)

x1 = np.array(["Trump", "Clinton"])
x2 = np.array(["Trump", "Clinton"])
pos1, neu1, neg1 = number_to_stat(1)
pos2, neu2, neg2 = number_to_stat(2)
y1 = np.array([pos1, pos2])
y2 = np.array([neg1, neg2])

sns.barplot(x=x1, y=y1, palette="rocket", ax=ax1)
ax1.axhline(0, color="k", clip_on=False)
ax1.set_ylabel("% positifs")

sns.barplot(x=x2, y=y2, palette="rocket", ax=ax2)
ax2.axhline(0, color="k", clip_on=False)
ax2.set_ylabel("% negatifs")

sns.despine(bottom=True)
plt.setp(f.axes)
plt.tight_layout(h_pad=2)
