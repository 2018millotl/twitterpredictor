from twitter_collect.fonc_3 import *

data = transform_to_dataframe(collect("squeezie OR Squeezie"))
print(len(data))


def retweet_min(rt_min, data):
    # renvoie une data contenant tous les tweets ayant été retweetés au moins rt_min fois

    rt = data[data.RTs >= rt_min].index
    result = data['tweet_textual_content'][rt]

    return result


def recherche_mots(mot, data):
    # renvoie une data contenant tous les tweets dont le texte contient mot

    index_rt = []

    # ajoute les index des tweets contenant mot à index_rt
    for ind in data.index:
        if mot in data["tweet_textual_content"][ind]:
            index_rt.append(ind)

    #crée une sous data contenant uniquement les indices de index_rt
    sous_data = data.loc[:, ["tweet_textual_content", "RTs"]]
    sous_data = sous_data.iloc[index_rt]

    return sous_data


def somme_rt_filtre(liste_mots, data):
    # fait le somme de tous les rt de tweets contenant un mot de liste_mot (les tweets avec plusieurs mots de la liste sont comptés plusieurs fois)

    compteur_rt = 0

    for mot in liste_mots:
        sous_data = recherche_mots(mot, data)
        for index in sous_data.index:
            compteur_rt += sous_data['RTs'][index]

    return compteur_rt


liste_mots_pos = ["bien", "bon", "heureux", "disruptif", "proactif", "honneur", "rofl", "xd", 'mdr', ]
liste_mots_neg = ["mensonge", "mensonge", "langue de bois", "batard", "fdp", "ras-le-bol", "gobe lobe", "mange-mort",
                  "salaud", "monstre"]

print(somme_rt_filtre(liste_mots_pos), somme_rt_filtre(liste_mots_neg))
