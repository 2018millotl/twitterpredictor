import pytest
import fonc_13_word_cloud as f13
import pandas as pd

def test_flatten():
    flat_liste_vide = f13.flatten([])

    assert flat_liste_vide == []

    liste = [[], [], []]
    flat_liste_listes_vides = f13.flatten(liste)

    assert flat_liste_listes_vides == []

    liste =  [[[1]], [[1], [2]]]
    flat3 = f13.flatten(liste)

    assert flat3 == [[],[],[]]

    liste = [[1,2,3],[1,2,3],[7,8],[]]
    flat4 = f13.flatten(liste)

    assert flat4 == [1,2,3,1,2,3,7,8]

def test_split_multiple():

    texte = "ceci est une phrase. En voilà une autre!Pas,unephrase"

    separateurs = " "
    mots1 = f13.split_multiple(separateurs, texte)

    assert mots1 == ["ceci", "est", "une", "phrase.", "En", "voilà", "une", "autre!Pas,unephrase"]

    separateurs = " ."
    mots2 = f13.split_multiple(separateurs, texte)

    assert mots2 == ["ceci", "est", "une", "phrase", "En", "voilà", "une", "autre!Pas,unephrase"]

    separateurs = " .!,"
    mots3 = f13.split_multiple(separateurs, texte)

    assert mots3 == ["ceci", "est", "une", "phrase", "En", "voilà", "une", "autre", "Pas","unephrase"]

def test_ensemble_mots_tweets():

    dict_list =[
        {"tweet_textual_content":"Ceci est une phrase"}
    ]


    data = pd.DataFrame(dict_list)

    mots = f13.ensemble_mots_tweets(data)

    assert mots == ['Ceci', "est", "une", "phrase"]

    dict_list = []
    data = pd.DataFrame(dict_list)

    mots = mots = f13.ensemble_mots_tweets(data)

    assert mots == []

    dict_list = [
        {'tweet_textual_content':"Ceci est une phrase"},
        {'tweet_textual_content':"ça aussi"},
        {'tweet_textual_content': "c.de!,la;merde"}
    ]
    data = pd.DataFrame(dict_list)
    mots = f13.ensemble_mots_tweets(data)

    assert mots == ["Ceci",'est','une','phrase','ça','aussi','c','de','la','merde']







