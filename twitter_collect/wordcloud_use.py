from wordcloud import (WordCloud, get_single_color_func)
import matplotlib.pyplot as plt
from twitter_collect.fonc_8 import *


class SimpleGroupedColorFunc(object):
    """Create a color function object which assigns EXACT colors
       to certain words based on the color to words mapping

       Parameters
       ----------
       color_to_words : dict(str -> list(str))
         A dictionary that maps a color to the list of words.

       default_color : str
         Color that will be assigned to a word that's not a member
         of any value from color_to_words.
    """

    def __init__(self, color_to_words, default_color):
        self.word_to_color = {word: color
                              for (color, words) in color_to_words.items()
                              for word in words}

        self.default_color = default_color

    def __call__(self, word, **kwargs):
        return self.word_to_color.get(word, self.default_color)


class GroupedColorFunc(object):
    """Create a color function object which assigns DIFFERENT SHADES of
       specified colors to certain words based on the color to words mapping.

       Uses wordcloud.get_single_color_func

       Parameters
       ----------
       color_to_words : dict(str -> list(str))
         A dictionary that maps a color to the list of words.

       default_color : str
         Color that will be assigned to a word that's not a member
         of any value from color_to_words.
    """

    def __init__(self, color_to_words, default_color):
        self.color_func_to_words = [
            (get_single_color_func(color), set(words))
            for (color, words) in color_to_words.items()]

        self.default_color_func = get_single_color_func(default_color)

    def get_color_func(self, word):
        """Returns a single_color_func associated with the word"""
        try:
            color_func = next(
                color_func for (color_func, words) in self.color_func_to_words
                if word in words)
        except StopIteration:
            color_func = self.default_color_func

        return color_func

    def __call__(self, word, **kwargs):
        return self.get_color_func(word)(word, **kwargs)


# data = transform_to_dataframe(collect("trump OR Trump"))

def data_to_texte(data):
    '''prend en fonction une data et renvoie le texte des tweets et des listes pos et
    neg comprenant les mots positifs et negatifs trouves dans ces tweets'''
    text = ""
    txt = data['tweet_textual_content']
    for sentence in txt:
        text += sentence
    text_blob = TextBlob(text)
    wordlist = text_blob.words
    '''wordlist est la liste de tout les mots des tweets de data'''
    list = []
    pos = []
    neg = []
    for mot in wordlist:
        a = TextBlob(str(mot))
        if a.sentiment.polarity > 0.01:
            list.append(str(a).lower())
            pos.append(str(a).lower())
        elif a.sentiment.polarity < -0.01:
            list.append(str(a).lower())
            neg.append(str(a).lower())
    text = ""
    for word in list:
        text += word + " "
    return text, pos, neg


def exe(string):
    data=transform_to_dataframe(collect(string))
    text, pos, neg = data_to_texte(data)
    # Since the text is small collocations are turned off and text is lower-cased
    wc = WordCloud(collocations=False,width=2*1920, height=2*1080).generate(text.lower())
    print(text)
    color_to_words = {
        # words below will be colored with a green single color function. They are the positive words
        '#00ff00': pos,
        # will be colored with a red single color function. They are the negative words
        'red': neg
    }

    # Words that are not in any of the color_to_words values
    # will be colored with a grey single color function
    default_color = 'grey'

    # Create a color function with single tone
    # grouped_color_func = SimpleGroupedColorFunc(color_to_words, default_color)

    # Create a color function with multiple tones
    grouped_color_func = GroupedColorFunc(color_to_words, default_color)

    # Apply our color function
    wc.recolor(color_func=grouped_color_func)

    # Plot
    plt.figure(figsize=(40,40))
    plt.imshow(wc, interpolation="bilinear")
    plt.axis("off")
    plt.show()
