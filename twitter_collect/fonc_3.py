import tweepy
import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# We import our access keys:
from twitter_collect.credentials import *

"""contient les premières fonctionnalités (1 à 4): 
twitter_setup() : se connecter à twitter
collect(txt) : récupérer une liste des tweets contenant un mot
store_tweets(tweets, filename) : les enregistrer dans un fichier
tweet_to_dict(tweet) : transforme un tweet en ditionnaire
transform_to_data : créer une data à partir d'une liste de tweets 
get_retweets(num_candidate) : renvoie un dictionnaire avec comme clés le texte d'un tweet et en données sont nombres de retweets"""

def twitter_setup():
    """
    Utility function to setup the Twitter's API
    with an access keys provided in a file credentials.py
    :return: the authentified API
    """
    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    # Return API with authentication:
    api = tweepy.API(auth)
    return api


def collect(txt):
    # renvoie les derniers tweets contenant tweet

    connexion = twitter_setup()
    tweets = connexion.search(txt, language="english", rpp=1000, count=1000)
    return tweets


def collect_2(txt):
    """ renvoie une liste des textes des derniers tweets contenants txt"""
    result = []
    connexion = twitter_setup()
    tweets = connexion.search(txt, language="french", rpp=100)
    for tweet in tweets:
        result.append(tweet.text)
    return result


def collect_tweet_candidate(num_candidate, file_path='CandidateData' + '\\'):
    """
    Generate and return a list of string queries for the search Twitter API from the file file_path_num_candidate.txt
    :param num_candidate: the number of the candidate
    :param file_path: the path to the keyword and hashtag
    files
    :param type: type of the keyword, either "keywords" or "hashtags"
    :return: (list) a list of string queries that can be done to the search API independently
    """
    try:
        liste_tweets = []

        with open(file_path + 'hashtag_candidate_' + str(num_candidate) + '.txt', 'r') as fichier:
            for line in fichier:
                liste_tweets.append(collect_2(line))

        with open(file_path + 'keywords_candidate_' + str(num_candidate) + '.txt', 'r') as fichier:
            for line in fichier:
                liste_tweets.append(collect_2(line))

        return liste_tweets

    except IOError:
        print("impossible d'ouvrir le fichier")


#  'CandidateData' + '\\'

def get_retweets(num_candidate):
    """renvoie un dictionnaire avec comme clés le texte du tweet, et comme données les nombres de retweets, pour tous les tweets en lien avec un cnadidat"""

    dict = {}
    file_path = 'CandidateData' + '\\'
    connexion = twitter_setup()

    with open(file_path + 'hashtag_candidate_' + str(num_candidate) + '.txt', 'r') as fichier:
        for line in fichier:
            tweets = connexion.search(line, rpp=1000)
            for tweet in tweets:
                txt = tweet.text
                nb_retweet = tweet.retweet_count
                dict[txt] = nb_retweet

    return dict


def store_tweets(tweets, file_name):
    # enregistre tweets dans un fichier json

    with open(file_name, 'w')as test_file:
        compt = 1
        for tweet in tweets:
            print(tweet)
            aux = 'tweet n' + str(compt) + ':' + tweet + '\n'
            json.dump(aux, test_file)
            compt += 1


def tweet_to_dict(tweet):
    # transforme un tweet en un dictionnaire

    return {"tweet_textual_content": str(tweet.text),
            "len": len(tweet.text),
            "likes": tweet.favorite_count,
            "RTs": tweet.retweet_count}


def transform_to_dataframe(tweets):
    # renvoie une data contenants les informations des tweets passés en arguments
    dict_list = []

    for tweet in tweets:
        dict_list.append(tweet_to_dict(tweet))

    data_frame = pd.DataFrame(dict_list)

    return data_frame
