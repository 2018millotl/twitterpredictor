import dash
import dash_core_components as dcc
import dash_html_components as html
from twitter_collect.fonc_8 import *

pos1, neu1, neg1 = string_to_stat("Trump OR trump OR donaldtrump")
pos2, neu2, neg2 = string_to_stat("Clinton OR clinton OR hillaryclinton")

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}

app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    html.H1(
        children='MVP : Prévoir une élection',
        style={
            'textAlign': 'center',
            'color': colors['text']
        }
    ),

    html.Div(children='un premier projet plutot quali wallah', style={
        'textAlign': 'center',
        'color': colors['text']

    }),

    html.Img(src="https://us-east-1.tchyn.io/snopes-production/uploads/2017/10/vic-berger-trump.jpg",width=200,height=300),
    html.Img(src="http://media.koreus.com/201606/hillary-clinton-donut-trou.jpg", width=200,height=300),

    dcc.Graph(
        id='example-graph-2',
        figure={
            'data': [
                {'x': [1, 2, 3], 'y': [pos1, neu1, neg1], 'type': 'bar', 'name': 'Trumpy'},
                {'x': [1, 2, 3], 'y': [pos2, neu2, neg2], 'type': 'bar', 'name': u'Hillary'},
            ],
            'layout': {
                'plot_bgcolor': colors['background'],
                'paper_bgcolor': colors['background'],
                'font': {
                    'color': colors['text']
                }
            }
        }
    )
])

if __name__ == '__main__':
    app.run_server(debug=True)
